grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (struct)+ EOF!;
    
struct 
    : stat_nl
    | loop
    ;
            
loop 
    : FOR LP stat logicalExpresion stat RP OB (struct)+ CB -> ^(FOR stat logicalExpresion stat (struct)+)
    | WHILE LP logicalExpresion RP OB (struct)+ CB -> ^(WHILE logicalExpresion (struct)+ )
    | DO OB (struct)+ CB WHILE LP logicalExpresion RP -> ^(DO logicalExpresion (struct)+)
    ;    
     
stat_nl
    :
    stat NL-> stat
    | NL->
    ;

stat
    : expr -> expr
    | VAR ID (PODST expr) -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID -> ^(VAR ID)
    | ID PODST expr -> ^(PODST ID expr) 
    ;


logicalExpresion 
    : expr
    (EQUAL^ expr
    | NOTEQUAL^ expr
    | GREATER^ expr
    | GREATEROREQUAL^ expr
    | LESS^ expr
    | LESSOREQUAL^ expr
    )*
    ;
            
expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

PRINT: 'print';

VAR :'var';

FOR : 'for';

WHILE : 'while';

DO : 'do';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

EQUAL 
  : '=='
  ;

NOTEQUAL
  : '!='
  ;
      
GREATER 
  : '>'
  ;
        
GREATEROREQUAL 
  :  '>='
  ;
        
LESS 
  : '<'
  ;
     
LESSOREQUAL 
  : '<='
  ;

OB
  : '{'
  ;
  
CB
  : '}'
  ;
  
LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
