tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer loopId=0;
  Integer conditionId=0;
  String declarations= new String();
}

prog : (e+=expr | e+=loop | decl)* -> program(name={$e},declarations={declarations}) ;

decl : ^(VAR i1=ID) {globals.newSymbol($ID.text); declarations+="DD "+$ID.text+'\n';}
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}
   
loop : ^(FOR init=expr cond=logicalExpresion action=expr (body+=expr|body+=loop|decl)*) {loopId++;} -> forLoop(loopId={loopId.toString()},init={$init.st},cond={$cond.st},action={$action.st},body={$body})
    | ^(WHILE cond=logicalExpresion (body+=expr|body+=loop|decl)*) {loopId++;} -> whileLoop(loopId={loopId.toString()},cond={$cond.st},body={$body})
    | ^(DO cond=logicalExpresion (body+=expr|body+=loop|decl)*) {loopId++;} -> doWhileLoop(loopId={loopId.toString()},cond={$cond.st},body={$body})
    ;
    
logicalExpresion : ^(EQUAL e1=expr e2=expr) {conditionId++;}  -> compare(conditionId={conditionId.toString()},instruction={"JE"},p1={$e1.st},p2={$e2.st})
    | ^(NOTEQUAL e1=expr e2=expr) {conditionId++;} -> compare(conditionId={conditionId.toString()},instruction={"JNE"},p1={$e1.st},p2={$e2.st})
    | ^(GREATER e1=expr e2=expr) {conditionId++;} -> compare(conditionId={conditionId.toString()},instruction={"JG"},p1={$e1.st},p2={$e2.st})
    | ^(GREATEROREQUAL e1=expr e2=expr) {conditionId++;} -> compare(conditionId={conditionId.toString()},instruction={"JGE"},p1={$e1.st},p2={$e2.st})
    | ^(LESS e1=expr e2=expr) {conditionId++;} -> compare(conditionId={conditionId.toString()},instruction={"JL"},p1={$e1.st},p2={$e2.st})
    | ^(LESSOREQUAL e1=expr e2=expr) {conditionId++;} -> compare(conditionId={conditionId.toString()},instruction={"JLE"},p1={$e1.st},p2={$e2.st})
    ;
    
expr    : ^(PLUS  e1=expr e2=expr)              -> add(p1={$e1.st},p2={$e2.st})
    | ^(MINUS e1=expr e2=expr)              -> substract(p1={$e1.st},p2={$e2.st})
    | ^(MUL   e1=expr e2=expr)              -> multiply(p1={$e1.st},p2={$e2.st})
    | ^(DIV   e1=expr e2=expr)              -> divide(p1={$e1.st},p2={$e2.st})
    | ^(PODST i1=ID   e2=expr) {globals.getSymbol($ID.text);}-> assign(n={$i1.text}, val={$e2.st})
    | INT  {numer++;}                       -> int(i={$INT.text},j={numer.toString()})
    | ID   {globals.getSymbol($ID.text);}   -> id(n={$ID.text})
    ;
    